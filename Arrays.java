/* Task:
1. Создать массив на 20 чисел.
2. Ввести в него значения с клавиатуры. (не буду этого делать, сделаешь сам).
3. Создать два массива на 10 чисел каждый.
4. Скопировать большой массив в два маленьких: половину чисел в первый маленький, вторую половину во второй маленький.
5. Вывести второй маленький массив на экран, каждое значение выводить с новой строки.
*/

// Needless stuff so far.
// import java.util.Scanner;

public class Arrays {

    // constants
    static final int MAX = 20;
    static final int HALF_MAX = MAX / 2;

    public static void main(String[] args) {
        int [] hugeArray = new int[MAX];
        int [] arr1 = new int[HALF_MAX];
        int [] arr2 = new int[HALF_MAX];

        // initialize array
        initArray(hugeArray);

        copyToArrays(hugeArray, arr1, arr2);
        printElements(arr2, HALF_MAX);

    }

    public static void initArray(int[] arr) {
        for(int i = 0; i < MAX; ++i) {
            arr[i] = i;
        }
    }

    public static void copyToArrays(int[] src, int[] dst1, int[] dst2) {
        for(int i = 0; i < HALF_MAX; i++) {
            dst1[i] = src[i];
            dst2[i] = src[i + HALF_MAX];
        }
    }

    public static void printElements(int [] arr, int size) {
        for(int i = 0; i < size; i++)
            System.out.printf("Element %d = %d\n", i, arr[i]);
    }
}
