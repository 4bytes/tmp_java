/* Task:
Массивы 7:
1. Создай массив строк.
2. Считай с клавиатуры 5 строк и добавь в массив.
3. Используя цикл, найди самую длинную строку в массиве.
4. Выведи найденную строку на экран. Если таких строк несколько,
   выведи каждую с новой строки.
*/

import java.util.Scanner;

public class LongestLines {
    private static Scanner scanner = new Scanner(System.in);
    static final int MAXLINES = 5;

    public static void main(String[] args) {
        String[] strings = new String[MAXLINES];
        int len = initArray(strings);
        printWords(strings, len);
    }

    private static int initArray(String[] array) {
        System.out.println("Type in 5 strings...");
        int strlen = 0;
        int maxline = 0;

        for(int i = 0; i < MAXLINES; i++) {
            array[i] = scanner.nextLine();
            strlen = array[i].length();

            if(strlen > maxline)
                maxline = strlen;
        }
        // remove or comment out before submitting to the teacher
        System.out.println("DBG >>> Max length of the word seen: " + maxline);
        return maxline;
    }

    private static void printWords(String[] words, int len) {
        for(int i = 0; i < MAXLINES; i++) {
            if(words[i].length() == len)
                System.out.printf("%s\n", words[i]);
        }
    }
}
